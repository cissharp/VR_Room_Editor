﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(SteamVR_TrackedController))]
public class MyController : MonoBehaviour {

    private SteamVR_TrackedController device;
    private SteamVR_Teleporter teleporter;
    private Transform playerRig;
    private GameObject marker;

    Transform reference
    {
        get
        {
            var top = SteamVR_Render.Top();
            return (top != null) ? top.origin : null;
        }
    }

    void Awake()
    {
        device = GetComponent<SteamVR_TrackedController>();
        marker = GameObject.Find("Marker");
        marker.SetActive(false);
        device.TriggerClicked += Trigger;
        device.PadClicked += Pad;
    }

    private void Update()
    {
        Ray ray = new Ray(device.transform.position, device.transform.forward);
        RaycastHit hit;

        EventSystem.current.SetSelectedGameObject(null);

        //Check to see if we should highlight any buttons
        if (Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("UI")))
        {
            Button btn = hit.collider.GetComponent<Button>();
            if (btn)
            {
                btn.Select();
            }
        }

        //If touching the touchpad, show where we'll teleport to when pressing touchpad
        if (device.padTouched)
        {
            if (Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("Floor")))
            {
                marker.SetActive(true);
                marker.transform.position = hit.point;
            } else
            {
                marker.SetActive(false);
            }
        } else
        {
            marker.SetActive(false);
        }
    }

    void Trigger(object sender, ClickedEventArgs e)
    {
        Ray ray = new Ray(device.transform.position, device.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            Button btn = hit.collider.GetComponent<Button>();
            if (btn != null)
            {
                btn.onClick.Invoke();
            }

            Toggle tgl = hit.collider.GetComponent<Toggle>();
            if(tgl != null)
            {
                tgl.isOn = !tgl.isOn;
            }

            SpawnedItem si = hit.collider.GetComponent<SpawnedItem>();
            if (si != null)
            {
                si.MakeMeARB();
                si.GetComponent<Rigidbody>().AddForceAtPosition(ray.direction * -100, hit.point);
            }
        }
    }

    void Pad(object sender, ClickedEventArgs e)
    {
        var t = reference;
        if (t == null)
            return;

        Ray ray = new Ray(device.transform.position, device.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, LayerMask.GetMask("Floor")))
        {
            Vector3 headPosOnGround = new Vector3(SteamVR_Render.Top().head.position.x, t.position.y, SteamVR_Render.Top().head.position.z);
            t.position = t.position + (ray.origin + (ray.direction * hit.distance)) - headPosOnGround;
        }
    }
}
