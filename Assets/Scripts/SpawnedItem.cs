﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnedItem : MonoBehaviour {

    public void Start()
    {
        SphereCollider collider = gameObject.AddComponent<SphereCollider>();

        Vector3 b;
        if(this.GetComponent<MeshFilter>() != null)
        {
            b = GetComponent<MeshFilter>().sharedMesh.bounds.extents;
            collider.radius = Mathf.Max(b.x, b.y, b.z) * .9f;
        }
        else
        {
            MeshFilter[] mf = GetComponentsInChildren<MeshFilter>();
            if(mf != null)
            {
                b = mf.OrderBy(m => m.sharedMesh.triangles.Count()).First().sharedMesh.bounds.extents;
                collider.radius = Mathf.Max(b.x, b.y, b.z) * .9f;
            }
        }
        collider.material = Resources.Load<PhysicMaterial>("Fruit");
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.name.Equals("Controller"))
        {
            MakeMeARB();
        }
    }

    public void MakeMeARB()
    {
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        Destroy(this);
    }
}
