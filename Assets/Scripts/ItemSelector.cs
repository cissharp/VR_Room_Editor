﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSelector : MonoBehaviour {
    public Transform itemList;
    public Transform spawnLocation;
    public List<ObjectColorPair> items = new List<ObjectColorPair>();

    [Serializable]
    public class ObjectColorPair {
        public GameObject myObject;
        public Color myColor = Color.white;
    }

    private GameObject currentGameObject = null;

    public void PopulateItemListGUI()
    {
        StartCoroutine(MakeNewButtons());
    }

    IEnumerator MakeNewButtons()
    {
        foreach (ObjectColorPair item in items)
        {
            CreateNewListItem(item);
            yield return new WaitForSeconds(.04f);
        }
    }

    void CreateNewListItem(ObjectColorPair item)
    {
        GameObject listItemButton = GameObject.Instantiate(Resources.Load("Prefabs\\ListItemButton")) as GameObject;
        listItemButton.transform.SetParent(itemList);
        listItemButton.transform.Find("Text").GetComponent<Text>().text = item.myObject.name;
        listItemButton.transform.localScale = Vector3.one;
        listItemButton.transform.localPosition = Vector3.zero;
        listItemButton.transform.localRotation = Quaternion.Euler(0,0,0);

        Button btn = listItemButton.GetComponent<Button>();
        btn.image.color = item.myColor;
        if(btn != null)
            btn.onClick.AddListener(() => Spawn(item.myObject));
    }

    void Spawn(GameObject itemName)
    {
        if(currentGameObject != null)
        {
            Destroy(currentGameObject);
            currentGameObject = null;
        }
        currentGameObject = GameObject.Instantiate(itemName);
        currentGameObject.transform.position = spawnLocation.position;
        currentGameObject.transform.rotation = spawnLocation.rotation;
        currentGameObject.transform.SetParent(this.transform);
    }

    public void ToggleItemList()
    {
        itemList.gameObject.SetActive(!itemList.gameObject.activeSelf);
    }
}
